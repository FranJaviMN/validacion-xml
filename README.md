# Validacion-xml
En este fichero vamos a ver que es la validacion de un fichero de formato xml y como podemos hacer estas validaciones.
## ¿Que es una validacion de un fichero XML?
La validacion de un fichero xml no es solo comprobar que la estructura que tiene el fichero xml es correcto, sino que cumpla una serie de normas a la que llamamos validacion.
La validacion que vamos a hacer va a ser mediante un fomato de validaciones llamado XSD. XSD es un formato para definir la estructura de un documento XML. Para poder crear validaciones vamos a ver las siguientes explicaciones.

### Tipos de elementos simples.
Los tipos de elementos nos indica de que tipo tiene que ser el elemento para que este sea valido, para ello usamos los siguientes tipos:

* **xs:string**: Nos indica que el elemento tiene que ser de tipo cadena de caracteres.
* **xs:decimal**: Nos indica que el elemento tiene que ser de tipo decimal.
* **xs.integer**: Nos indica que el elemento tiene que ser de tipo entero.
* **xs:boolean**: Nos indica que el elemento tiene que ser de tipo boleano.
* **xs:date**: Nos indica que el elemento tiene que ser una fecha. La fecha tiene el siguiente formato: *YYYY-MM-DD*
* **xs:time**: Nos indica que el elemento tiene que ser una hora. La hora tiene el siguiente formto: *hh:mm:ss*
* **Ejemplo**
    > <xs:attribute name="lang" type="xs:string" use="required"/> 
    > 
    > Nos indica que el nombre debe ser 'lang', de tipo cadena de caracteres y tiene que ser obligatorio.

    ><xs:attribute name="nota" type="xs:decimal">
    >
    > Nos indica que el nombre debe ser 'nota' y de tipo decimal
    
    > <xs:attribute name="fecha" type="xs:date">
    >
    > Nos indica que el nombre es 'fecha' y de tipo fecha. Su formato seria, por ejemplo: *2020-10-01*

### Restricciones 
El formato de validaciones de XSD nos permite añadir unas restricciones a los posibles valores que vayamos a validar en nuestro fichero xml. Dichas restricciones se pueden establecer en diferentes aspectos, llamados facetas.
Dicho de otro modo, las facetas permiten definir restricciones sobre los posibles valores de atributos o elementos. Las facetas que pueden usarse son:

* **xs:legth** Podemos especificar una longitud fija, es decir, la longitud del dato debe ser de ese valor especificado.
* **xs:minLength** Podemos especificar una longitud minima que debe tener el dato que introduzcamos. La sintaxis es la siguiente:
    > **<xs:minLength value="num_minimo">**
* **xs:maxLength** Podemos especificar una longitud maxima que puede tener el dato que introduzcamos. L sintaxis es la siguiente:
    > **<xs:maxLength value="num_maximo">**

    * **Ejemplo** En el siguiente ejemplo vamos a establecer un elemento llamado *prueba* con la restriccion de que el valor que pueda tener no pueda ser mayor de 20 caracteres pero no menor de 5 caracteres:
    ~~~XSD
    <xs:element name="prueba">
        <xs:simpleType>
            <xs:restriction base="xs:interger">
                <xs:maxLength value="20"/>
                <xs:minLength value="5"/>
            </xs:restiction>
        </xs:simpleType>
    </xs:element>
    ~~~
    * **\<xs:simpleType\>** Nos permite definir un tipo simple y especifiacr sus restricciones.
    * **\<xs:restriction\>** Nos permite definir restricciones de *\<xs:simpleType\>*.
* **xs:pattern** Nos permite definir unos caracteres que seran los datos que son permitidos. Su sintaxis es la siguiente:
    > **<xs:pattern value="Expresion_regular">**
    * **Ejemplo** En el siguiente ejemplo queremos que un campo de tipo *string* llamado *prueba* solo pueda contener dos letras comprendidas desde la **A** hasta la **F**:
    ~~~XSD
    <xs:element name="prueba">
        <xs:simpleType>
            <xs:restiction base="xs:string">
                <xs:pattern value="[A-F][A-F]"/>
            </xs:restriction>
        </xs:simpleType>
    </xs:element>
    ~~~
* **xs:enumeration** Nos permite definir un valor o una lista de valores que son admitidos: Su sintaxis es la siguiente:
    > **<xs:enumeration value="Valor_admitido1">**
    * **Ejemplo** En el siguiente ejemplo vamos a definir un campo llamado *prueba* y solo pueda tener los valores *"Verde"*, *"Rojo"* y *"Amarillo"*.
    ~~~XSD
    <xs:element name="prueba">
        <xs:simpleType>
            <xs:restriction base="xs:string">
                <xs:enumeration value="Verde"/>
                <xs:enumeration value="Rojo"/>
                <xs:enumeration value="Amarillo"/>
            </xs:restiction>
        </xs:simpleType>
    </xs:element>
    ~~~
* **xs:whitespace** Nos permite especificar como se debe tratar a los posibles espacios en blanco, tabulaciones, saltos de linea y los retornos de carro que puedan aparecer. Su sintaxis es la siguiente:
    > **<xs:whitespace values='opciones'>**
    >
    Las opciones que podemos poner en whitesapace son:
    * "preserve"= Le indicamos que tanto los espacios en blanco, los retornos de carro y las tabulaciones se mantengan.
    * "replace"= Sustituye las tabulaciones, los saltos de linea y los retornos de carro por espacios en blancos.
    * "collapse"= Despues de reemplazar todas las tabulaciones, los saltos de liena y los retornos de carro por espacios en blanco, eliminar los espacios en blanco unicos y sustituir varios espacios en blanco seguidos por un unico espacio en blanco.
* **xs:maxInclusive** Nos permite especificar que el valor debe ser menor o igual que el indicado